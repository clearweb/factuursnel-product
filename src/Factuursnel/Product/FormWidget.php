<?php namespace Factuursnel\Product;

use Clearweb\Clearwebapps\Form\MultipleUploadsField;
use Clearweb\Clearwebapps\Form\SelectField;

class FormWidget extends \Clearweb\Clearwebapps\Eloquent\FormWidget
{
	function init() 
	{
		parent::init();
		
		$this->getForm()
			->replaceField(
						   'images',
						   with(new MultipleUploadsField)
						   ->setName('images')
						   
						   )
            ->moveField('ean', 3)
			;
		
		return $this;
	}
}