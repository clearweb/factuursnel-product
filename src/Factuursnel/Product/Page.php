<?php namespace Factuursnel\Product;

use Clearweb\Clearwebapps\Layout\OverviewLayout;

use Clearweb\Clearwebapps\Page\Page as BasePage;

class Page extends BasePage
{
	function __construct() {
		$this->setSlug(trans('nav.products'));
		$this->setTitle(trans_choice('app.product', 2));
	}
	
	function init() {
		$this->setLayout(new OverviewLayout)
			->addWidgetLinear(new FormWidget, 'right', 1)
			->addWidgetLinear(new ListWidget, 'left', 1)
			;
		
		
		
		return parent::init();
	}
}