<?php namespace Factuursnel\Product;

class ListWidget extends \Clearweb\Clearwebapps\Eloquent\SortableListWidget
{
	function init()
	{
        $this->setModelClass('\Factuursnel\Product\Product');
        
		parent::init();
		
		$this->setShowNewButton(true);
		
		$this->getList()
			->hideColumn('description')
			->hideColumn('images')
            ->removeColumn('ean')
            ->replaceColumn(
                            'price',
                            function ($row) {
                                return '&euro; '.number_format($row['price'], 2, ',', '.');
                            }
                            )
            ->addActionLink(new DeleteActionLink, 'delete')
			;
		
		return $this;
	}
}