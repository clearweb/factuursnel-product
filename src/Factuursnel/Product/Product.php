<?php namespace Factuursnel\Product;

use Factuursnel\CustomPrice\CustomPrice;

use DB;

Class Product extends \Eloquent
{
	protected $table = 'product';
	protected $fillable = array('name', 'ean', 'price', 'images', 'description', 'vat_id');
    
    public function getClientCustomPrice($clientId)
    {
        $customPrice = CustomPrice::where('client_id', $clientId)
            ->where('product_id', $this->id)
            ->pluck('custom_price')
            ;
        
        if (empty($customPrice)) {
            $customPrice = false;
        }
        
        return $customPrice;
    }
    
    public function hasClientCustomPrice($clientId)
    {
        return ($this->getClientCustomPrice($clientId) != false);
    }
    
    public function vat()
    {
        return $this->belongsTo('\Factuursnel\Base\Vat\Vat');
    }
}
