<?php namespace Factuursnel\Product;

class DeleteActionLink extends \Clearweb\Clearwebapps\Eloquent\DeleteActionLink
{
    function init() {
		$this->setDeleteAction(new DeleteAction());
		
		return parent::init();
	}
}